#! /usr/bin/julia

using ArgParse
using TextAnalysis
using TopicModeling
using Gadfly
using DataFrames

function run(args)

    # Build corpus
    path = args["corpus"]
    println("Reading corpus from location: $(path)")
    corpus = DirectoryCorpus(path)

    # Update internal structures
    println("Building index")
    update_lexicon!(corpus)
    update_inverse_index!(corpus)

    # Compute term document matrix
    println("Building term document matrix")
    m = DocumentTermMatrix(corpus)

    k = args["topic"]
    topwords = args["topwords"]
    documents = map(x -> split(x.filename, "/")[end], corpus.documents)
    models = [lda, nmf, lsa]

    println()
    println("> Options are:")
    println("K:\t\t$(k)")
    println("topwords:\t$(topwords)")
    println("V:\t\t$(size(m.terms, 1))")
    println("D:\t\t$(size(corpus.documents, 1))")
    println("Models: $(models)")
    println()

    # Run each model on the corpus
    for model in models
        println("> Topic modeling with $(model)")
        result = model(m, k)

        println("Final perplexity: $(perplexity(m, result))")
        println()

        # Launch a benchmark
        if args["bench"]
            nbRuns = 10
            mean_time = 0.0

            for i = 1:nbRuns
                t0 = time()
                model(m, k)
                mean_time += time() - t0
            end

            println("mean time (seconds): ", mean_time / nbRuns)
        else
            # Extract topics
            topics = getTopics(result, m.terms, k, topwords)
            for (i, topic) in enumerate(topics)
                if minimum(topic.coeffs) != maximum(topic.coeffs)
                    draw(PNG("$(model)/topic$(i)_$(k).png", 15inch, 10inch), plot(x = topic.words, y = topic.coeffs, Geom.bar))
                end
            end

            # Print to stdout.
            if args["stdprint"]
                println("> The best $(topwords) topwords are:")
                println(topics)
            end

            # Print topics
            docs = {}
            topics = {}
            coeffs = {}
            for (i, d) in enumerate(documents)
                docs = [docs, repmat([d], k)]
                topics = [topics, reshape([1:k], k)]
                coeffs = [coeffs, reshape(mixture(result)[i, :], k)]
            end
            df = DataFrame(Doc = docs, Topic = topics, Coeff = coeffs)
            draw(PNG("$(model)/mixture.png", 20inch, 10inch), plot(df, x="Topic",  y="Coeff", color="Doc", Geom.line))


            # Cluster documents
            if args["clustering"]
                clustering = cluster(result, documents, 5)
                println("> Clustering results:")
                println(clustering)
            end
            println()
        end
    end
end



function main(args)
    s = ArgParseSettings()

    s.description = "Topic extraction"
    s.version = "Version 0.42"

    @add_arg_table s begin
        "--bench", "-b"
            help = "Launch benchmark"
            action = :store_true
        "--corpus", "-c"
            help = "Path to the corpus"
            arg_type = String
            default = "./corpus"
        "--topic", "-t"
            help = "Number of topics to extract"
            arg_type = Int
            default = 10
        "--topwords", "-w"
            help = "Number of words to extract from each topic"
            arg_type = Int
            default = 5
        "--stdprint"
            help = "Print the results in the console"
            action = :store_true
        "--clustering"
            help = "Print the clustering results in the console"
            action = :store_true
    end

    parsed_args = parse_args(s)
    run(parsed_args)
end


# Call main with command-line arguments
main(ARGS)
