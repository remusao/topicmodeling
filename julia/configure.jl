#! /usr/bin/julia

# Installed packages
installed = keys(Pkg.installed())

# Add requiered packages
deps = ["Gadfly", "Cairo", "ArgParse", "DataFrames"]
for d in deps
    Pkg.add(d)
end

if !in("TopicModeling", installed)
    Pkg.clone("https://github.com/LanguageProcessingUnit/TopicModeling.jl.git")
else
    warn("TopicModeling already installed, remove it before running configure.jl")
end

if !in("TextAnalysis", installed)
    Pkg.clone("https://github.com/remusao/TextAnalysis.jl.git")
else
    warn("TextAnalysis already installed, remove it before running configure.jl")
end

Pkg.update()
