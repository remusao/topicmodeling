
from operator import itemgetter

import nimfa

class NMF(object):

    def __init__(self, tdm, documents, index):
        self.tdm = tdm
        self.documents = documents
        self.index = index


    def getTopics(self, nbTopics, nbWords):
        # Compute matrix factorization
        model = nimfa.mf(self.tdm.T,
                  seed = "random_vcol",
                  rank = nbTopics,
                  method = "nmf",
                  max_iter = 15,
                  initialize_only = True,
                  update = 'divergence',
                  objective = 'div')
        fit = nimfa.mf_run(model)

        self.basis = fit.basis()
        self.coef = fit.coef()

        features = self.index.get_feature_names()
        topics = []
        for topicId in xrange(self.basis.shape[1]):
            # Extract topic
            topWords = sorted(enumerate(self.basis[:, topicId].todense().ravel().tolist()[0]), key=itemgetter(1), reverse=True)[:nbWords]
            topics.append([(features[idx], coeff) for idx, coeff in topWords][::-1])

        return topics


    def clusterDocuments(self, k):
        pass
