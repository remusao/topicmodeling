from numpy import *
from numpy.linalg import norm
from .newton_raphson import newton_raphson

from numba import autojit

#@autojit
def parameter_estimation(gam, phi):
    """
    Things to know:
    -   M = # of documents ;
    -   K = # of topics ;
    -   N = # of words ;
    -   V = size of vocabulary.
    """

    K = gam.shape[1]

    # Iterate over all the documents and construct beta matrix.
    beta = sum(phi, axis=0).T

    # We normalize beta for each topic.
    for i in xrange(0, K):
        if norm(beta[i, :]) != 0:
            beta[i, :] /= norm(beta[i, :], 1)

    # Update alpha.
    alpha = newton_raphson(gam)
    alpha /= norm(alpha, 1)

    return alpha, beta
