from .likelihood import likelihood


from numba import autojit

#@autojit
def total_likelihood(g, p, a, b, w):
    """
    Things to know:
    -   M = # of documents ;
    -   K = # of topics ;
    -   N = # of words ;
    -   V = size of vocabulary.

    For the total likelihood:
    -   g = MxK ;
    -   p = MxNxK ;
    -   a = K ;
    -   b = KxV ;
    -   w = MxNxV.
    """

    M = g.shape[0]
    l = 0

    for d in xrange(0, M):
        l += likelihood(g[d].T, p[d], a, b, w[d])

    return l
