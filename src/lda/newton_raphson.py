from __future__ import print_function

from numpy import *
from numpy.linalg import norm
from scipy.special import *

from numba import autojit

#@autojit
def newton_raphson(gammas):
    """
        Compute the newton & raphson method.
    """

    M, K = gammas.shape

    if M <= 1:
        alpha = gammas[0, :].T
        return alpha

    ini_alpha = mean(gammas, axis=0) / K

    maxiter = 20
    alpha = ini_alpha.copy()
    palpha = zeros(K)

    for t in xrange(0, maxiter):
        f = M * (psi(sum(alpha)) - psi(alpha)) + sum(psi(gammas), axis=0) - sum(psi(sum(gammas, axis=1)), axis=0)
        df = M * polygamma(1, alpha) - polygamma(1, sum(alpha))
        alpha += f / df

        # If any alpha is negative, we try again.
        # There is no sense of a negative parameter for the Dirichlet
        # distribution.
        if any(alpha < 0):
            ini_alpha /= 10
            alpha = ini_alpha.copy()
            palpha = zeros(K)
            continue

        # Test of convergence if we already have done an iteration.
        if t > 1 and norm(alpha - palpha, 1) / norm(alpha, 1) < 10e-4:
            break

        palpha = alpha

    return alpha
