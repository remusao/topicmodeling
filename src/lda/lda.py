from numpy import zeros, inf, sort, flipud, sum
from numpy.linalg import norm
from numpy.random.mtrand import rand
from .inference import variational_inference
from .perplexity import perplexity
from .estimation import parameter_estimation

import pickle


from numba import autojit

#@autojit
def lda(K, words, max_iter):
    """
    Train the Latent Dirichlet Allocation model on the corpus of
    documents.

    M = number of documents.
    K = number of topics.
    N = number of words.
    """

    M = words.shape[0]
    V = words.shape[1]

    # We first need to create vectors from our text corpus.
    phi = zeros((M, V, K))
    gam = zeros((M, K))

    # Initialize alpha and beta.
    beta = rand(K, V)

    for i in xrange(0, K):
        beta[i, :] /= norm(beta[i, :], 1)

    alpha = flipud(sort(rand(K)))
    alpha /= norm(alpha, 1)

    t_conv = 1
    old_t_conv = -inf
    it = 0
    # We iterate until convergence.
    while norm(t_conv - old_t_conv) / norm(t_conv) > 10e-4 and it < max_iter:
        # E-Step: variational inference, for each document.
        for d in xrange(0, M):
            phi[d], gam[d] = variational_inference(alpha, beta, words[d, :])

        # M-Step: parameter estimation.
        alpha, beta = parameter_estimation(gam, phi)

        # Convergence update.
        it += 1
        old_t_conv = t_conv
        t_conv = perplexity(words, beta, gam)
        print("LDA | it:", it, "; ppl:", sum(t_conv))

    return alpha, beta, gam, phi


class LDA(object):

    def __init__(self, tdm, documents, index):
        self.tdm = tdm.toarray()
        self.documents = documents
        self.index = index


    def getTopics(self, nbTopics, nbWords):

        DB_path = 'LDA/DB_lda_%d' % nbTopics
        #try:
        #    f = open(DB_path, 'r')
        #    [alpha, beta, gamma, phi] = pickle.load(f)
        #    f.close()
        #except:
        #    alpha, beta, gamma, phi = lda(nbTopics, self.tdm, 100)
        #    f = open(DB_path, 'w')
        #    pickle.dump([alpha, beta, gamma, phi], f)
        #    f.close()
        alpha, beta, gamma, phi = lda(nbTopics, self.tdm, 100)

        print("LDA | Final perplexity of the model: ", sum(perplexity(self.tdm, beta, gamma)))

        res = []
        #for i in xrange(0, nbTopics):
        #    res.append([[self.index.get_feature_names()[j], beta[i, j]] for j in beta[i, :].argsort()[:-nbWords - 1: -1]])
        return res


    def clusterDocuments(self, k):
        pass
