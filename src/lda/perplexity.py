from numpy import log, dot
from numpy.linalg import norm
from numpy import exp, sum


from numba import autojit

#@autojit
def perplexity(words, beta, gamma):
    M = gamma.shape[0]
    s = sum(sum(words))

    egamma = gamma.copy()
    # Normalize gamma.
    for i in range(0, M):
        egamma[i] /= norm(egamma[1], 2)

    ppl = exp(-sum(words * log(dot(egamma, beta)), axis=1) / s)

    return ppl
