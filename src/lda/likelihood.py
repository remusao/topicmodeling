from math import log
from numpy import *
from numpy.linalg import norm
from scipy.special import psi, gamma


from numba import autojit

#@autojit
def likelihood(g, p, a, b, w):
    """
    Compute the loglikelihood of a single document.

    Things to know:
    -   K = number of topics ;
    -   N = number of words ;
    -   V = size of vocabulary.

    Properties:
    -   words: each row is a bit-vector of size V where sum(a row) = 1, and
        the i-th bit in the vector which is equal to 1 says the word is the
        i-th in the vocabulary.

    Parameters:
    -   g = gamma, matrix of K elts ;
    -   p = phi, matrix of VxK elts ;
    -   a = alpha, matrix of K elts ;
    -   b = beta, matrix of KxV elts ;
    -   w = words, matrix of VxM elts.
    """

    K = p.shape[1]
    V = p.shape[0]

    sum_a = sum(a)
    sum_g = sum(g)
    gam_term = psi(g) - psi(sum_g)

    # Formulas from paper
    l = log(gamma(sum_a)) - sum(log(gamma(a))) + sum((a - 1) * gam_term)
    l += sum(p * tile(gam_term, (V, 1)))

    for k in xrange(0, K):
        for v in xrange(0, V):
            if b[k, v] != 0:
                l += p[v, k] * log(b[k, v])

    l += -log(gamma(sum_g)) + sum(log(gamma(g))) - sum((g - 1) * gam_term)

    for v in xrange(0, V):
        for k in xrange(0, K):
            if p[v, k] != 0:
                l -= w[v] * (p[v, k] / norm(p[v, :], 1)) * log(p[v, k] / norm(p[v, :], 1))

    return l
