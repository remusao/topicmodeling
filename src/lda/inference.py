from numpy import zeros, sum, tile, inf, exp, ones
from numpy.linalg import norm
from scipy.special import psi
import numpy
from .likelihood import likelihood


from numba import autojit

#@autojit
def variational_inference(alpha, beta, w):
    """
    Optimize the free varational parameters phi and gamma
    considering the logLikelihood of the document.
    Things to know:
    - K = number of topics ;
    - N = number of words.

    % Properties:
    - words: each row is a bit-vector of size V where sum(a row) = 1, and
        the i-th bit in the vector which is equal to 1 says the word is the
        i-th in the vocabulary.

    - phi: sum(n,:) = 1.
    """

    K = alpha.shape[0]
    V = w.shape[0]
    # If we don't test with a raw term frequency, we prefer counting the non zeros.
    N = sum(w)

    # Init phi and gamma vectors.
    gam = alpha + (N / K)
    phi = zeros((V, K))

    l = gam
    old_l = zeros(K)
    # Loop until convergence.
    while norm(l - old_l) / norm(l) > 10e-3:
        # Variational inference.
        phi = (beta * tile(w, (K, 1)) * tile((exp(psi(gam) - psi(sum(gam, axis=0)))), (V, 1)).T).T

        #phi /= sum(phi, axis=1)[:, numpy.newaxis]
        for v in xrange(0, V):
            if norm(phi[v, :]) != 0:
                phi[v, :] /= norm(phi[v, :], 1)

        phi = phi * tile(w, (K, 1)).T

        # gamma = alpha + sum over n of phi.
        gam = (alpha.T + sum(phi, axis=0)).T

        # Update our convergence considering the logLikelihood.
        old_l = l
        l = gam

    return phi, gam
