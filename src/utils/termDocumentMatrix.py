
import sklearn.feature_extraction.text as feature

def count(urls):
    vectorizer = feature.CountVectorizer(input='filename')
    return (vectorizer, vectorizer.fit_transform(urls))

def tf_idf(urls):
    vectorizer = feature.TfidfVectorizer(max_features = 1000, input = 'filename')
    return (vectorizer, vectorizer.fit_transform(urls))
