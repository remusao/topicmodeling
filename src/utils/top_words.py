from pylab import barh, yticks, show, figure, xlabel, ylabel, grid, title, savefig


def top_words(words, nbWords, nbtopics, comment):
    width = 0.5
    fig_path = "OUT/documents_T%d_basisW%d_%s.png"

    figure(1)
    for i in range(0, nbtopics):
        # Plot topic
        barh(range(0, nbWords), [prob for (n, prob) in words[i]], color="yellow", align="center")
        yticks(range(0, 10))
        yticks([k + width / 2 for k in range(0, 10)], [n for (n, p) in words[i]])
        xlabel("Weight")
        ylabel("Term")
        title("Highest Weighted Terms in Basis Vector W%d/%d" % (i + 1, nbtopics))
        grid(True)
        savefig(fig_path % (nbtopics, i + 1, comment), bbox_inches="tight")