
from __future__ import unicode_literals
import os


def getDocumentsURL(path):
    files = []

    for f in os.listdir(path):
        # Check that it's a regular file and the extension is .corpus
        if not os.path.isdir(f) and f.endswith('.preproc'):
            files.append(path + '/' + f)

    return files
