
from math import sqrt
from numpy import count_nonzero

def thumbRule(tdm):
    return sqrt(tdm.shape[0] / 2.0)

def textBasedHeuristic(tdm):
    m, n = tdm.shape
    return (m * n) / len(tdm.data) #count_nonzero(tdm)

def elbowRule(tdm):
    pass

