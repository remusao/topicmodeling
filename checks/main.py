#! /usr/bin/python2

from __future__ import print_function, division, absolute_import, unicode_literals
from sys import argv
import sys
sys.path.append('../src/')

# Import methods for topic extraction
from nmf.nmf import NMF
from lda.lda import LDA

# Topic extraction utils
from utils.termDocumentMatrix import count, tf_idf
from utils.top_words import top_words
from utils.corpus import getDocumentsURL

import argparse
import time
import pickle


# Debug
import traceback


def check(corpus, args):

    # idf = (index, tdm)
    # tf = (index, tdm)
    documents, idf, tf = corpus

    nbTopics = args.topic
    nbWords = args.topwords

    # Print information about the test sessions
    if args.corpus:
        print('From corpus:\t', args.corpus[0])
    else:
        print('From dump:\t', args.tdm[0])
    print('|Corpus|:\t', len(documents))
    print('|tf_index|:\t', len(tf[0].get_feature_names()))
    print('|idf_index|:\t', len(idf[0].get_feature_names()))
    print('nb topics:\t', nbTopics)
    print('nb top words:\t', nbWords)


    methods = [('LDA', tf)]

    for m, (index, tdm) in methods:

        print()
        print('Running tests with:', m)

        # Create model
        model = globals()[m](tdm, documents, index)

        mean_time = 0.0
        for i in xrange(0, 10):
            t0 = time.time()

        #try:
        #    # Extracting topics
            topics = model.getTopics(nbTopics, nbWords)
        #    top_words(topics, nbWords, nbTopics, "LDA")
        #except AttributeError:
        #    print('Unable to extract topics with:', m)


            mean_time += time.time() - t0
        print('timing:', mean_time / 10.0)




def main(args):

    corpus = None

    # Get term document matrix
    if args.tdm:
        path = args.tdm[0]
        if path.endswith('.tdm'):
            with open(path, 'rb') as f:
                corpus = pickle.load(f)
        else:
            raise NameError('Erreur: dump extension')
    elif args.corpus:
        path = args.corpus[0]
        documents = getDocumentsURL(path)
        tf_index, tf = count(documents)
        idf_index, idf = tf_idf(documents)
        documents = [d.split('/')[-1] for d in documents]
        corpus = documents, (idf_index, idf), (tf_index, tf)
        # Dump corpus
        with open('corpus.tdm', 'w') as f:
            pickle.dump(corpus, f)
    else:
        raise NameError('Erreur: missing corpus')

    check(corpus, args)


if __name__ == '__main__':

    #
    # Argument parser
    #

    parser = argparse.ArgumentParser(description='Topic extraction')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('-d', '--tdm', nargs = 1,  help = 'term document matrix')
    group.add_argument('-c', '--corpus', nargs = 1,  help = 'paths to corpus')

    parser.add_argument('-l', '--loop', type = int,  help = 'loop over number of topics', default = 1)
    parser.add_argument('-t', '--topic', type = int,  help = 'number of topics', default = 2)
    parser.add_argument('-w', '--topwords', type = int,  help = 'number of words to extract', default = 5)

    main(parser.parse_args())
