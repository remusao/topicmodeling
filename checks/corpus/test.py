#! /usr/bin/python2

import os
import sys

def getTokens(f):
    categories = set(['numeric', 'verb:pastpart', 'verb:prespart', '*noun:common', 'noun:common', 'verb:plain', 'adj', 'noun:propernoun', 'noun:acronym'])
    stopwords = ["a","able","about","across","after","all","almost","also","am","among","an","and","any","are","as","at","be",
                "because","been","but","by","can","cannot","could","dear","did","do","does","either","else","ever","every",
                "for","from","get","got","had","has","have","he","her","hers","him","his","how","however","i","if","in",
                "into","is","it","its","just","least","let","like","likely","may","me","might","most","must","my","neither",
                "no","nor","not","of","off","often","on","only","or","other","our","own","rather","said","say","says","she",
                "should","since","so","some","than","that","the","their","them","then","there","these","they","this","tis",
                "to","too","twas","us","wants","was","we","were","what","when","where","which","while","who","whom","why",
                "will","with","would","yet","you","your","."," ","1","2","3","4","5","6","7","8","9","0", "during", "changes",
                "(1)","(2)","(3)","(4)","(5)","(6)","(7)","(8)","(9)","usually","involved","labeled"]
    tokens = []

    for line in f:
        tags = line.split('\t')

        if len(tags) > 3:
            category = set(tags[1].split())
            lemma = tags[2]

            if category.issubset(categories) and (lemma not in stopwords):
                tokens.append(lemma)

    return tokens

def main(path):
    # Read each file
    for f in os.listdir(path):
        # Check that it's a regular file and the extension is .corpus
        if not os.path.isdir(f) and f.endswith('.corpus'):
            abs_path = path + '/' + f
            with open(abs_path, 'r') as i:
                with open(f[:-7] + '.preproc', 'w') as out:
                    out.write('\n'.join(getTokens(i)))


if __name__ == '__main__':
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        print('%s file' % sys.argv[0])
