
\section{Modèles génératifs}

\subsection{Principe général}

\paragraph{}
Très souvent, les données que nous obtenons ne veulent rien dire et sont dénuées de sens et d'ordres. Sans ces notions essentielles, il ne sert à rien de travailler avec. Une manière de donner du sens à ces données est de trouver le processus génératif qui a permis de les construire. Ainsi, il suffirait de partir de ce processus pour construire un modèle.

\paragraph{}
Par exemple, si on se demandait comment une photo était prise, on pourrait imaginer le processus suivant :

\begin{enumerate}
	\item choisir une quantité N d'objets qui apparaitront sur la photo.
	\item choisir une profondeur de champs.
	\item parmis les scènes possibles, choisir la position $(x,y)$ qui va correspondre le plus à nos critères précédents.
	\item se placer en $(x,y)$ et prendre la photo.
\end{enumerate}

\paragraph{}
De manière assez simple, un modèle génératif permet de modéliser un ensemble de données en fonction de ses paramètres cachés. Pour reprendre notre exemple précédent, les paramètres cachés seraient la profondeur de champs et l'ensemble des prises possibles. Tandis que la variable observable serait la photo prise au final (et par effet de bord la quantité d'objet N apparaissant sur la photo).

\paragraph{}
Contrairement à leurs homologues discriminatifs, les modèles génératifs se basent sur des probabilités jointes pour modéliser leurs problèmes. Ce principe de prendre toutes les variables en compte lors de la modélisation, au lieu de seulement les spécifiées avec des conditions, a pour but d'obtenir de meilleurs corrélations entre les variables latentes et observables.

\paragraph{}
Une manière commune, et intuitive, de résoudre un modèle génératif est de maximiser la vraissemblance du modèle. C'est ce que nous utiliserons afin de résoudre le modèle de David M. Blei~: Latent Dirichlet Allocation.

\subsection{Latent Dirichlet Allocation}

\paragraph{}
Latent Dirichlet Allocation est un modèle génératif et probabiliste pour des collections de données discrètes. LDA est également un modèle hiérarchique Bayésien à trois niveaux :

\begin{figure}
\begin{center}
\includegraphics[scale=0.6]{topics/images/3-levels_hierarchical_baysian_model.png}
\caption{Forme arborescente de LDA}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=0.6]{topics/images/Smoothed_LDA.png}
\caption{Autre représentation de LDA}
\end{center}
\end{figure}

\newpage
\paragraph{}
Comme on peut le voir sur les précédentes représentations, LDA modélise les documents comme des mélanges aléatoires sur des thématiques cachées, où chaque thématique est une distribution de probabilité sur les mots.

\subsubsection{Processus génératif}

\begin{enumerate}
	\item Choisir $N \sim Poisson(\xi)$.
	\item Choisir $\theta \sim Dir(\alpha)$.
	\item Pour chacun des $N$ mots $w_n$ :
	\begin{enumerate}
		\item Choisir une thématique $z_n \sim Multinomial(\theta)$.
		\item Choisir un mot $w_n$ de $p(w_n|z_n,\beta)$, une probabilité multinomiale conditionnée sur la thématique $z_n$.
	\end{enumerate}
\end{enumerate}

Où $\beta$ est une matrice telle que $\beta_{i,j} = p(w^j = 1 | z^i = 1)$.

\paragraph{}
Contrairement à beaucoup d'autres modèles, LDA propose des mélanges de thématiques. Ce qui veut dire qu'un mot peut appartenir à plusieurs thématiques avec différentes proportions, il en est de même avec les documents et finalement le corpus de documents.

\paragraph{}
Afin de mieux visualiser le processus, on peut voir les différentes variables comme suit :

\begin{itemize}
	\item $\alpha$ représente les proportions des thématiques au sein du corpus de textes~;
	\item $\beta$ représente l'importance des mots pour les thématiques~;
	\item $\theta$ représente les proportions des thématiques au sein de chaque document~;
	\item $z_n$ représente les quantités d'informations, concernant les thématiques, que va porter le mot $w_n$.
\end{itemize}

\paragraph{}
Après résolution du modèle, il est facile de déterminer quels sont les mots les plus représentatifs des thématiques et ne garder que ces derniers dans les documents. C'est pourquoi LDA peut être vu comme une technique de réduction de dimension.

\subsubsection{Propriétés intéréssantes}

Une propriété intéressante, tirée directement de la distributin de Dirichlet, est que l'ensemble des valeurs est contenu dans un simplex ouvert de dimension $(K - 1)$, où K est le nombre de thématique donné. En conséquence, elle pourrait nous permettre de voir la distribution de Dirichlet comme une métrique.

\paragraph{}
Une seconde propriété intéressante est que LDA respecte les notions d'échangeabilités. Ce qui veut dire que l'ordre des mots et des documents n'a aucune importance, facilitant grandement encore les calculs et le modèle.

\subsubsection{Algorithme et fonctionnement}

\paragraph{}
Pour des raisons que nous n'aborderons pas dans le présent document, on ne peut pas obtenir directement les distributions à posteriori des variables latentes. C'est pourquoi nous devons revenir à un modèle plus simple pour pouvoir approximer ces distributions.

\paragraph{}
Le modèle que nous choisissons d'étudier est celui des distributions variationnelles.

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.15]{topics/images/approximate_model_LDA.png}
\caption{Représentation graphique de la distribution variationnelle utilisée dans LDA}
\end{center}
\end{figure}

\paragraph{}
Afin d'approximer au mieux le modèle LDA, on minimise la divergence de Kullback-Leibler\footnotemark entre l'à posteriori du modèle approximé et le vrai à posteriori. Puis, sans rentrer dans les détails, l'application de l'inégalité de Jensen permet d'obtenir la borne inférieure du log de vraisemblance de notre distribution variationnelle. Enfin, maximiser cette borne inférieure équivaut à minimiser la divergence de Kullback-Leibler.

\footnotetext{La divergence de Kullback-Leibler $D_{KL}(Q||P)$ mesure la quantité d'information perdue quand Q approxime P (P et Q étant deux distributions de probabilités).}

\begin{center}
	\begin{algorithm}[H]
		\SetAlgoLined
		\DontPrintSemicolon
		\KwData{\\
		$K$ : nombre de thématique\\
		$V$ : taille du vocabulaire\\
		$w$ : matrice d'occurence des mots}
		\Begin{
		$\beta \leftarrow$ initialisation aléatoire\;
		$normalisation(\beta)$\;
		\;
		$\alpha \leftarrow$ initialisation aléatoire\;
		$normalisation(\alpha)$\;
		\Repeat{borne inférieur du log de vraisemblance non maximisée}{
			\tcc{Inférence des variables latentes}
			\For{$d \in$ Documents}{
				$\gamma_i \leftarrow \alpha_i + N_d/K$, $\forall i$\;
				\Repeat{borne inférieur du log de vraisemblance non maximisée}{
					\For{n = 1 à $N_d$}{
						\For{i = 1 à $K$}{
								$\phi_{d,n,i} \leftarrow \beta_{i,v} exp\big(\psi(\gamma_i - \psi(\sum\nolimits_{j=1}^k \gamma_j\big)$\;
								$normalisation(\phi_{d,n})$														}
						}
						$\gamma \leftarrow \alpha + \sum\nolimits_{n=1}^N \phi_{d,n} * w_{d}$\;
					}
				}
			\tcc{Estimation des paramètres}
			$\beta_{i,j} \leftarrow \sum\limits_{d=1}^M \sum\limits_{n=1}^{N_d} \phi_{d,n,i} * w_{d,n}$, $\forall (i,j) \in K\times V$\;
			$normalisation(\beta)$\;
			\;
			$alpha \leftarrow newton\_raphson(\gamma)$\;
			$normalisation(\alpha)$
		}
		}
		\caption{Algorithme de LDA avec l'inférence variationnelle}
	\end{algorithm}
\end{center}