
\section{Introduction}

\subsection{Enjeux}

\paragraph{}
Nous vivons à une époque où l'information abonde, sous toutes formes, et sur
tous supports. En particulier, l'arrivée d'Internet a permis la création et la mise
à disposition de quantités d'informations. Ces quantités ne cessent de croître et
elles sont telles qu'il est impossible pour un être humain de les traiter.

\paragraph{}
Google annonçait récemment la numérisation de 130~millions de livres. Chaque jour,
des millions de \textit{tweets} sont postés, des millions de nouvelles pages sont créées
et des centaines d'actualités sont propulsées sur la \emph{toile}. Mais
cela ne constitue qu'une infime partie de ce qui peut être trouvé sur Internet.
Des questions se posent alors :
\begin{itemize}
	\item Comment extraire les informations latentes contenues dans cet
	amoncellement de données ?
	\item Comment structurer automatiquement ces données ?
\end{itemize}

\paragraph{}
Les moteurs de recherche sont un premier pas en faveur d'une utilisation
des ressources de l'Internet par les êtres humains, mais de nombreuses
pistes sont encore à explorer. Notamment, les techniques permettant
l'extraction de la sémantique contenue dans les textes n'en sont qu'à
leurs balbutiements.


\paragraph{}
Pour toutes ces raisons, les recherches menées sur les thèmes de l'extraction
de l'information, de la classification, et de manière plus générale,
du traitement du langage naturel sont des enjeux majeurs pour la science.


\subsection{Analyse des composantes}

\paragraph{}
Avant de parler des méthodes utilisées pour traiter le langage naturel,
extraire des thématiques ou classifier des documents, il est important de
prendre conscience que les problèmes auxquels nous sommes confrontés
appartiennent à une classe beaucoup plus générale de problématiques.
En effet, nombreuses
sont les situations où un phénomène est observé, des données sont collectées,
sans que l'on ait accès à une compréhension profonde des mécanismes qui
expliquent les données que nous avons en notre possession. Quelques exemples
pour bien comprendre :
    \subparagraph{Personne}
    Lorsque nous observons une personne notre cerveau est capable de détecter
    qu'elle dispose de bras, de jambes, etc. Mais si ce même individu
    est pris en photo, sans aucune forme d'analyse, il est a priori
    impossible de préciser de quelles parties est composée son corps,
    tout ce que nous avons est un ensemble de pixels possédant chacun une
    certaine couleur.
    \subparagraph{Météo}
    Lorsque l'on observe le temps qu'il fait, nous obtenons des mesures
    de température, de pression, de vitesse du vent, nous pouvons observer
    qu'il pleut ou qu'il fait soleil. Mais nous n'avons pas connaissance
    des \emph{saisons} qui pourraient expliquer ces observations.
    \subparagraph{Document}
    Lorsque nous lisons un texte, ce que nous observons est un ensemble de
    mots. Sans aucune analyse poussée, il n'est pas possible de dire
    immédiatement quelles sont les thématiques, traitées par le document,
    qui expliquent la présence des mots en question.
    

\paragraph{}
De manière générale, un objet quelconque est composé de parties, et chaque
partie contribue à la totalité de l'objet en question. Considérant que l'on
peut lister les différentes parties composant un certain objet, la formule
suivante exprime la composition de ce dernier :
$$ \text{Objet} =
    \text{Partie}_1
    + \text{Partie}_2 + \cdots
    + \text{Partie}_n$$

\paragraph{}
Éventuellement, toutes les parties d'un objet n'ont pas une contribution d'importance
égale. Ce que l'on peut exprimer par :
$$ \text{Objet} =
    \alpha_1~\text{Partie}_1
    + \alpha_2~\text{Partie}_2 + \cdots
    + \alpha_n~\text{Partie}_n$$
Où les $\alpha_i$ expriment le poids de la contribution de chaque partie à
l'objet total. Par exemple si notre objet est une voiture nous aurions :
$$ \text{Voiture} =
    4~\text{roue}
    + 4~\text{portières} + \cdots
    + \text{moteur}$$


\paragraph{}
On remarque donc que le problème de trouver la structure interne d'un ensemble
de données, ou d'expliquer les variables latentes qui expliquent l'observation
est un problème très générale, que l'on rencontre dans de nombreux domaines :
\begin{itemize}
    \item traitement d'images~;
    \item reconnaissance des formes~;
    \item prévision de la météo~;
    \item reconnaissance de caractères~;
    \item extraction de thématiques~;
    \item classification~;
    \item etc.
\end{itemize}


\paragraph{}
L'analyse de documents, et plus particulièrement l'extraction de thématiques
est une instance particulière de cette classe de problèmes, puisqu'on peut
considérer un document comme une mixture de thématiques :
$$ \text{Document} =
    \alpha_1~\text{Thème}_1
    + \alpha_2~\text{Thème}_2 + \cdots
    + \alpha_n\text{Thème}_n $$
Où les $alpha_i$ sont des poids indiquant la proportion du sujet $i$ dans
le document. Par exemple un article traitant de politique à $70$\% et d'écologie
à $30$\% serait exprimé comme :
$$\text{Article} = 0.7~\text{Politique} + 0.3~\text{Écologie}$$
Grâce à cette remarque nous nous rendons compte que la multitude
d'outils existants dans des domaines aussi divers que le traitement d'image
ou la classification automatique pourra être réutilisée dans le cadre
du traitement du langage naturel.

\paragraph{}
Dans la suite de ce rapport nous présenterons diverses techniques dont
l'objectif est d'exhiber la décomposition en thématiques d'un document ou
d'un ensemble de documents. La connaissance de cette structure latente
pourra avoir des applications intéressantes telles que : la classification
des documents, la détection de nouvelles tendances, la découverte de
catégories cachées, etc.
