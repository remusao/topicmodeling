\documentclass[12pt]{beamer}

\usetheme{Sybila}
\usepackage[frenchb]{babel}
\usepackage{thumbpdf}
\usepackage{wasysym}
\usepackage{ucs}
\usepackage[utf8]{inputenc}
\usepackage{pgf,pgfarrows,pgfnodes,pgfautomata,pgfheaps,pgfshade}
\usepackage{verbatim}
\usepackage{subcaption}
\usepackage{color}
\usepackage{algorithm2e}

\setbeamertemplate{footline}[frame number]


\pdfinfo
{
  /Title       (PFE Presentation)
  /Creator     (Rémi Berson)
  /Author      (Rémi Berson)
}


\title{Extraction de thématiques}
\subtitle{Projet de fin d'études à EPITA}
\author{Rémi Berson \\ Vincent Latrouite \\ Fabrice Rougeau \\ Juliette Tisseyre \\ Olivier Querné}
\date{}

\begin{document}

\frame{\titlepage}

\section*{}
\begin{frame}
  \frametitle{Sommaire}
  \tableofcontents[hidesubsections]
\end{frame}

\AtBeginSection[]
{
  \frame<handout:0>
  {
    \frametitle{Sommaire}
    \tableofcontents[currentsection, hideallsubsections]
  }
}


\newcommand<>{\highlighton}[1]{%
  \alt#2{\structure{#1}}{{#1}}
}

\newcommand{\icon}[1]{\pgfimage[height=1em]{#1}}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Content starts here %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Corpus
\section{Corpus}

\begin{frame}{Corpus}
Articles \emph{Wikipédia} en anglais :
\begin{itemize}
    \item $530$ documents~;
    \item $16810$ termes (après pré-traitement)~;
    \item extraction de $50$ thématiques~;
    \item on garde $10$ mots par thématique.
\end{itemize}
\end{frame}


% Pre-traitement
\section{Pré-traitement}


\begin{frame}{Pré-traitement}
Basé sur l'annotation \textsc{Systran} :
\begin{itemize}
    \item Lemme~;
    \item sélection de la catégorie :
    \begin{itemize}
        \item verbe~;
        \item nom (propre, commun)~;
        \item adjectif
    \end{itemize}
    \item élimination des \emph{stop-words}.
\end{itemize}
\end{frame}

\begin{frame}{Term Document Matrix}
\begin{itemize}
    \item $D$ documents~;
    \item $V$ termes uniques~;
    \item Corpus $\in \mathcal{M}_{D, V}(\mathbb{R})$
\end{itemize}

$$ A_{d,v} =
 \begin{pmatrix}
  a_{1,1} & a_{1,2} & \cdots & a_{1,V} \\
  a_{2,1} & a_{2,2} & \cdots & a_{2,V} \\
  \vdots  & \vdots  & \ddots & \vdots  \\
  a_{D,1} & a_{D,2} & \cdots & a_{D,V}
 \end{pmatrix}$$
 
 $(a_{d, v})$ : coefficient associé au terme $v$ du document $d$ 
\end{frame}


\begin{frame}{Coefficients}

\begin{itemize}
    \item Nombre d'occurrences du mot \textit{(count)}~;
    \item fréquence du mot dans le document \textit{(frequency)}~;
    \item \emph{term-frequency inverse-document-frequency} \textit{(tf-idf)}
    $$\text{tf-idf}_{i, j} = \text{tf}_{i, j} \times
    \text{log}\left(\frac{D}{\text{occurence}_i}\right)$$
    \begin{itemize}
        \item mots très présents dans le corpus
        \textcolor{red}{\textbf{-- --}}
        \item mots peu présents dans le corpus
        \textcolor{green}{\textbf{++}}
    \end{itemize}
\end{itemize}
\end{frame}


% Topic extraction
% - NMF
% - LSI
% - LDA
\section{Extraction de thématiques}


\begin{frame}{Méthodes}
\begin{block}{Model reduction}
\begin{itemize}
    \item Latent Semantic Indexing \textit{(LSI)}
    \item Non-negative Matrix Factorization \textit{(NMF)}
\end{itemize}
\end{block}

\begin{block}{Generative model}
\begin{itemize}
    \item Latent Dirichlet Allocation \textit{(LDA)}
\end{itemize}
\end{block}

\end{frame}


\subsection{Latent Semantic Indexing}

\begin{frame}{Latent Semantic Indexing}
\begin{itemize}
    \item Décomposition en valeurs singulières~;
    \item $M = U . \Sigma . V^\intercal$~;
    \item réduction de dimension en espace sémantique~;
\end{itemize}
\end{frame}


\begin{frame}{Singular Value Decomposition}
Principe de la décomposition :
\begin{itemize}
	\item $M = U . \Sigma . V^\intercal$~;
	\item généralisation de la diagonalisation à des matrices quelconques~;
	\item $\Sigma$ matrice diagonale avec valeurs singulières~;
	\item découvre la structure interne~;
	\item trouver les axes expliquant au mieux les données.
\end{itemize}
\end{frame}


\begin{frame}{Singular Value Decomposition}
\begin{figure}
\includegraphics[width=\textwidth]{svd}
\end{figure}
\end{frame}


\begin{frame}{Latent Semantic Indexing}
Réduction de dimension :
\begin{itemize}
	\item Sélection des $k$ premières valeurs singulières ;
	\item sélection des $k$ vecteurs de $U$ et $V$ correspondants ;
	\item reconstruction de la matrice $M$ (débruitée, information dominante, moins de termes).
\end{itemize}
\end{frame}


\begin{frame}{Latent Semantic Indexing}
Interprétation\footnote{\textsc{Hoffman}, Probabilistic Latent Semantic Indexing.
In \emph{International Computer Science Institute} (1999)} :
\begin{description}
	\item[mixture] thématiques associées à chaque document \\
	$U = \mathcal{P}(d_i~|~z_k)_{i, k}$
	\medskip
	\item[topics] mots associés à chaque thématique \\
	$V = \mathcal{P}(w_j~|~z_k)_{j,k}$
	\medskip
	\item[proportions] $\Sigma = \mathcal{P}(z_k)$
\end{description}
\end{frame}


\begin{frame}{Latent Semantic Indexing}
\begin{itemize}
    \item utile pour \emph{classification}, \emph{indexation}, \emph{réduction de dimension}
    $$ [car, truck, flower] \rightarrow [1.3 \times car + 0.3 \times truck, flower] $$
    \item difficile à interpréter :
    $$ [car, bottle, flower] \rightarrow [1.3 \times car + 0.3 \times bottle, flower] $$
    \item ne préserve pas les propriétés des données
    \footnote{positivité des coefficients, interprétation physique}~;
    \item méthode trop générale pour extraire des thématiques.
\end{itemize}
\end{frame}

\subsection{Non-negative Matrix Factorization}

\begin{frame}{Non-negative Matrix Factorization}
\begin{itemize}
    \item Factorisation de matrices à \emph{coefficients positifs}~;
    \item respecte les propriétés des données (positivité)~;
    \item modèle peut être interprété directement~;
    \item pas de solution générale $\rightarrow$ \emph{approximation}~;
    \item $\approx$ \textit{probabilistic Latent Semantic Indexing}\footnote{Si on minimise la
    divergence de Kullback-Leibler}
\end{itemize}
\end{frame}

\begin{frame}{Non-negative matrix factorization}
\begin{itemize}
    \item $M \approx UV^\intercal$~;
    \item $M$, $U$, $V$ matrices non-négatives~;
    \item $U$ : thématiques dans chaque document~;
    \item $V$ : mots dans chaque thématique.
\end{itemize}
\end{frame}

\begin{frame}{Non-negative matrix factorization}
\begin{itemize}
    \item généralisation de \textit{KMeans}~;
    \item $U_i$ : un vecteur de nos données~;
    \item $C_j$ : un \textit{cluster}~;
    \item 
     $V_{i, j} = \left\{ 
        \begin{array}{ll}
          1 & \text{si}~U_i \in C_j \\
          0 & \text{sinon} \\
        \end{array} \right.$~;
    \item \textit{KMeans} : $\text{min}~||M - UV^\intercal||^2_{L_2}$
\end{itemize}
\end{frame}

\begin{frame}{Non-negative matrix factorization}
\begin{itemize}
    \item \textit{KMeans} : $V$ a des coefficients $v_{i, j} \in \{0, 1\}$~;
    \item \textit{NMF} : $V$ a des coefficients $v_{i, j} \in \mathbb{R}^+$~;
    \item plus flexible~;
    \item plus complexe à approximer.
\end{itemize}
\end{frame}


\subsection{Latent Dirichlet Allocation}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{block}{Introduction du modèle}
    \begin{itemize}
            \item Modèle génératif probabiliste~;
            \item équivalent à un modèle hiérarchique Bayésien à trois
                niveaux~;
            \item modélise les proportions de thématiques pour le corpus, les
                documents et les mots eux-même~;
            \item \og bag-of-words \fg : ordre des mots/documents sans importance~:
                \begin{itemize}
                        \item Propriété intéressante simplifiant les calculs.
                \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
	\begin{block}{Processus génératif}
		\begin{enumerate}
			\item Choisir la taille du document $N \sim Poisson(\xi)$
      \item Choisir une répartition $\theta \sim Dir(\alpha)$ des thématiques
        dans le document
      \item Pour chaque mot $w_n$ de 1 à N:
        \begin{enumerate}
          \item Choisir une thématique $z_n \sim Multinomiale(\theta)$
          \item Choisir un mot $w_n$ de $p(w_n|z_n,\beta)$, mélange de
            thématiques conditionné sur $z_n$
        \end{enumerate}
		\end{enumerate}
	\end{block}
  \begin{block}{Paramètres et variables latentes}
    \begin{itemize}
      \item Paramètres : $\alpha, \beta$~;
      \item variables latentes : $\theta, z_n$.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{block}{Inférence}
    \begin{itemize}
      \item Inférence des variables latentes en calculant $p(\theta, z|w,
        \alpha,\beta)$~;
      \item on veut récupérer $p(w|\alpha,\beta)$ afin d'estimer le maximum a
        posteriori de LDA~;
      \item marginalisation des variables latentes rendant le calcul de l'a
        posteriori impossible.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{figure}[h!]
    \includegraphics[scale=0.4]{images/LDA.png}
    \caption{Modèle LDA}
  \end{figure}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{block}{Inférence variationnelle}
    \begin{itemize}
      \item Introduction des paramètres variationnels $\phi, \gamma$ pour
        calculer $q(\theta,z|\phi,\gamma)$~;
      \item on maximise la borne inférieure
        du log de vraisemblance afin d'obtenir une meilleur approximation de
        la vraie distribution~:
        \begin{enumerate}
          \item Kullback-Leibler divergence pour comparer la vraie
            distribution et la variationnelle (minimisation)~,
          \item Inégalité de Jensen pour obtenir la borne inférieure du log
            de vraisemblance de la distribution variationnelle (maximisation).
        \end{enumerate}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{figure}[h!]
    \includegraphics[scale=0.2]{images/approximate_model_LDA.png}
    \caption{Distribution approximée pour l'inférence variationnelle}
  \end{figure}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{block}{Équations}
    \begin{itemize}
      \item Finalement, $(\gamma*, \phi*) = argmin_{\gamma, \phi}D(q(\theta,
        z|\gamma,\phi) || p(\theta,z|w,\alpha,\beta))$~;
      \item $\phi_{n,i} \propto \beta_{i, w_n} exp\big(\psi(\gamma_i) -
        \psi(\sum\limits_{j=1}^k\gamma_j)\big)$~;
      \item $\gamma_i = \alpha_i + \sum\nolimits_{n=1}^N\phi_{n,i}$.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{block}{Estimation des paramètres}
    \begin{itemize}
      \item Afin de maximiser au mieux le log de vraisemblance de notre modèle approché,
        nous devons estimer les paramètres $\beta$ et $\alpha$~;
      \item estimation de $\beta$ facile car analytique~;
      \item estimation de $\alpha$ impossible analytiquement.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{block}{Estimation de $\alpha$}
    \begin{itemize}
      \item Afin d'estimer $\alpha$, nous utilisons une descente de gradient
        avec (L correspond au log de vraisemblance du corpus)~:\\
        \begin{itemize}
          \item fonction~: $f = \frac{\partial L}{\partial \alpha_i}$,
          \item dérivée~: $\partial f = \frac{\partial L}{\partial \alpha_i
            \alpha_j}$.
        \end{itemize}
      \item la méthode utilisée est celle de Newton-Raphson.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{algorithm}[H]
    \SetAlgoLined
    Initialisation aléatoire de $\beta$ et $\alpha$\\
    \While{pas de convergence de l'estimation}{
      MLE en inférant $\gamma, \phi$\\
      MLE en estimant $\alpha, \beta$\\
    }
  \end{algorithm}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{block}{Nos choix pour l'implémentation}
    \begin{itemize}
      \item Utilisation d'un EM pour inférer $\gamma, \phi$~:
        \begin{itemize}
          \item décision de convergence si $\gamma$ ne change pas de plus de
            $10^{-4}$\%~;
        \end{itemize}
      \item décision de convergence de l'EM principale si la perplexité de
        notre modèle ne change pas de plus de $10^{-5}$\%~;
      \item implémentation de la version batch de l'algorithme.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Latent Dirichlet Allocation}
  \begin{block}{Pourquoi ces choix}
    \begin{itemize}
      \item Principalement pour l'accessibilité~;
      \item utilisation de la mesure de perplexité car plus rapide à calculer que le log de
        vraisemblance du corpus~;
      \item Newton-Raphson car complexité $O(n)$ grâce à la forme de la
        matrice Hessienne~;
      \item convergence si pas plus de $10^{-4}$\% ou $10^{-5}$\% pour copier au
        maximum les choix de l'auteur de LDA.
    \end{itemize}
  \end{block}
\end{frame}

% Post-traitement
\section{Post-traitement}

\begin{frame}{Post-traitement}
Résultat de l'extraction :
\begin{description}
    \item[Mixture] $\gamma \in \mathcal{M}_{D, K}(\mathbb{R})$ \\
    $$\gamma_{d, k} = \mathcal{P}(\text{Topic}_k ~|~ \text{Document}_d)$$
    \item[Thématiques] $\beta \in \mathcal{M}_{K, V}(\mathbb{R})$ \\
    $$\beta_{k, v} = \mathcal{P}(\text{Word}_v ~|~ \text{Topic}_k)$$
\end{description}
\end{frame}


\begin{frame}{Lissage des thématiques}
Lissage \textit{idf} sur les thématiques :
$$\text{term-score}_{k, v} = \hat{\beta}_{k, v} \times \text{log}\left(
\frac{\hat{\beta}_{k, v}}{\left(\prod_{j=1}^{K} \hat{\beta}_{j, v}\right)^{\frac{1}{K}}}
\right)$$
\end{frame}

\begin{frame}{Lissage des thématiques}
Le terme $\prod_{j=1}^{K} \hat{\beta}_{j, v}$ pose problème :
\begin{description}
    \item[problème] précision d'un \textit{float} (produit de probabilités)
    \item[solution 1] utiliser \textit{float} à précision arbitraire (lent)~;
    \item[solution 2] développer pour transformer en somme
\end{description}
$$\text{term-score}_{k, v} = \hat{\beta}_{k, v} \times \left(
\text{log}\left(\hat{\beta}_{k, v}\right)
- \frac{\sum_{j=1}^{K}\text{log}\left(\hat{\beta}_{j, v}\right)}{K}
\right)$$
\end{frame}


% Détails d'implémentation
\section{Implémentation}

\begin{frame}{Langages}
\begin{itemize}
    \item Matlab~;
    \item Python (Numpy, Scipy, Scikit)~;
    \item \textbf{Julia}
    \begin{itemize}
        \item Plus performant (compilateur \textsc{jit})~;
        \item très complet (bibliothèque standard, \textit{binding} \textsc{r})~;
        \item flexible (syntaxe Matlab, richesse de Python)~;
        \item encore jeune.
    \end{itemize}
\end{itemize}
\end{frame}


% Resultats
\section{Résultats}



% Dopage
\begin{frame}{Dopage}
  \begin{figure}
    \begin{tabular}{cc}
      \textsc{LDA} & \textsc{NMF} \\
      \hline               \\
      \textcolor{blue}{anabolic} & \textcolor{blue}{anabolic} \\
      \textcolor{blue}{athlete} & antidoping \\
      conservatee & \textcolor{blue}{athlete} \\
      conservatorship & \textcolor{blue}{drug} \\
      de & olympic \\
      \textcolor{blue}{drug} & race \\
      performance & \textcolor{blue}{sport} \\
      \textcolor{blue}{sport} & \textcolor{blue}{steroid} \\
      \textcolor{blue}{steroid} & substance \\
      u.s & use
    \end{tabular}
    \caption{Thématique dopage}
  \end{figure}
\end{frame}

\begin{frame}{LDA : dopage}
\begin{figure}
\includegraphics[width=\textwidth]{images/lda/dopage}
\caption{Thématique \emph{dopage} avec \textsc{lda}}
\end{figure}
\end{frame}

\begin{frame}{NMF : dopage}
\begin{figure}
\includegraphics[width=\textwidth]{images/nmf/dopage}
\caption{Thématique \emph{dopage} avec \textsc{nmf}}
\end{figure}
\end{frame}


% Musique groupe
\begin{frame}{Musique}
  \begin{figure}
    \begin{tabular}{cc}
      \textsc{LDA} & \textsc{NMF} \\
      \hline               \\
      Coles & David \\
      Sadie & Hodges \\
      \textcolor{blue}{album} & \textcolor{blue}{album} \\
      artist & award \\
      \textcolor{blue}{band} & \textcolor{blue}{band} \\
      \textcolor{blue}{best} & \textcolor{blue}{best} \\
      music & evanescance \\
      release & pop \\
      \textcolor{blue}{song} & \textcolor{blue}{song} \\
      Sharma & -
    \end{tabular}
    \caption{Thématique musique}
  \end{figure}
\end{frame}

\begin{frame}{LDA : musique}
\begin{figure}
\includegraphics[width=\textwidth]{images/lda/musique_groupe}
\caption{Thématique \emph{musique} avec \textsc{lda}}
\end{figure}
\end{frame}

\begin{frame}{NMF : musique}
\begin{figure}
\includegraphics[width=\textwidth]{images/nmf/musique_groupe}
\caption{Thématique \emph{musique} avec \textsc{nmf}}
\end{figure}
\end{frame}

% Homosexualité
\begin{frame}{Homosexualité}
  \begin{figure}
    \begin{tabular}{cc}
      \textsc{LDA} & \textsc{NMF} \\
      \hline               \\
      \textcolor{blue}{charter} & Canada\\
      \textcolor{blue}{discrimination} & \textcolor{blue}{charter}\\
      gay & \textcolor{blue}{discrimination}\\
      \textcolor{blue}{lgbt} & human\\
      \textcolor{blue}{marriage} & \textcolor{blue}{lgbt}\\
      \textcolor{blue}{orientation} & \textcolor{blue}{marriage}\\
      \textcolor{blue}{right} & \textcolor{blue}{orientation}\\
      section & \textcolor{blue}{right}\\
      \textcolor{blue}{sex} & \textcolor{blue}{sex}\\
      \textcolor{blue}{sexual} & \textcolor{blue}{sexual}\\
    \end{tabular}
    \caption{Thématique homosexualité}
  \end{figure}
\end{frame}

\begin{frame}{LDA : homosexualité}
\begin{figure}
\includegraphics[width=\textwidth]{images/lda/homo}
\caption{Thématique \emph{homosexualité} avec \textsc{lda}}
\end{figure}
\end{frame}

\begin{frame}{NMF : homosexualité}
\begin{figure}
\includegraphics[width=\textwidth]{images/nmf/homo}
\caption{Thématique \emph{homosexualité} avec \textsc{nmf}}
\end{figure}
\end{frame}


\begin{frame}{Mixture LDA}
\begin{figure}
\includegraphics[width=\textwidth]{images/lda/mixture}
\caption{Répartition des thématiques dans les documents avec \textsc{lda}. Chaque ligne est un document, thématique en abscisse, coefficient en ordonnée}
\end{figure}
\end{frame}

\begin{frame}{Mixture NMF}
\begin{figure}
\includegraphics[width=\textwidth]{images/nmf/mixture}
\caption{Répartition des thématiques dans les documents avec \textsc{nmf}. Chaque ligne est un document, thématique en abscisse, coefficient en ordonnée}
\end{figure}
\end{frame}

\begin{frame}{Mesures}
\begin{itemize}
    \item $3.2$ Go de mémoire vive~;
    \item $15$ minutes pour les 3 méthodes~;
    \item perplexité :
    \begin{description}
        \item[LDA] $535.56$
        \item[NMF] $534.32$
        \item[LSA] $531.94$
    \end{description}
\end{itemize}
\end{frame}

% Pistes d'amelioration
% - Nombre de thematiques dans un corpus
% - Online LDA
% - Hierarchical topic modeling
% - Application de demo avec des fonctions utiles (gestionnaire de news ?)

\section{Analyse des résultats}


\begin{frame}{Améliorations LSA}
\begin{itemize}
    \item Algorithme plus performance~;
    \item calcul de seulement $k$ valeurs singulières~;
    \item réduction de l'empreinte mémoire.
\end{itemize}
\end{frame}

\begin{frame}{Améliorations NMF}
Améliorations possibles :
\begin{itemize}
    \item Algorithme plus performance~;
    \item réduction de l'erreur d'approximation.
\end{itemize}
\end{frame}


\begin{frame}{Améliorations LDA (performances)}

    \begin{itemize}
      \item Remplacer l'\textsc{em} par plus performant~;
      \item possibilité d'ajouter du multi-threading~;
      \item passage de l'\textsc{em} globale de mode Batch à On-line.
    \end{itemize}

\end{frame}

\begin{frame}{Améliorations LDA (modèle)}

    \begin{itemize}
      \item Pas de corrélations entre les thématiques~:
        \begin{itemize}
          \item implémentation de CTM (Correlated Topic Modeling),
          \item implémentation de TPA (The Pachinko Allocation)~;
        \end{itemize}
      \item Paramètre fixé $K$ (nombre de thématiques à
        chercher)~:
        \begin{itemize}
          \item implémentation de HDP (Hierarchical Dirichlet Process),
          \item implémentation de HPP (Hierarchical Pachinko Process).
        \end{itemize}
    \end{itemize}

\end{frame}



\subsection{Nombre de clusters}

\begin{frame}{Nombre de thématiques}
    \begin{itemize}
    \item Modèle non-paramétrique~;
    \item heuristique.
    \end{itemize}
\end{frame}

\begin{frame}{Méthode non-paramétrique}
\begin{itemize}
    \item Hiérarchique \emph{(dendrogramme)}~;
    \includegraphics[width=0.8\textwidth]{images/dendrogramme}
\end{itemize}
\end{frame}

\begin{frame}{Heuristiques}
\begin{itemize}
	\item Rule of thumb : $k \approx \sqrt{\frac{n}{2}}$~;
	\item Elbow method~;
	\item Text : $k \approx \frac{D \times V}{\text{non-zero}}$~:
    \item Méthode basée sur un calcul en kernel-space~;
    \item piste à explorer.
\end{itemize}
\end{frame}



% Conclusion
\section{Conclusion}

\begin{frame}{Conclusion}
\begin{itemize}
	\item $3$ méthodes implémentées~;
	\item résultats intéressants~;
	\item nombreuses pistes d'amélioration :
	\begin{itemize}
		\item performance~;
		\item justesse des résultats~;
		\item modélisation hiérarchique.
	\end{itemize}
\end{itemize}
\end{frame}


\begin{frame}{Questions ?}
    \begin{center}
        \includegraphics[scale=0.3]{questions}
    \end{center}
\end{frame}

\end{document}
